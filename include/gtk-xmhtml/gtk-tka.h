/*****
* gtk-tka.h : Gtk/XmHTML Toolkit Abstraction Public Interface
*
* This file Version	$Revision$
*
* Creation date:		Mon Sep 28 08:49:25 CEST 1998
* Last modification: 	$Date$
* By:					$Author$
* Current State:		$State$
*
* Author:				XmHTML Developers Account
*
* Copyright (C) 1994-1998 by Ripley Software Development 
* All Rights Reserved
*
* This file is part of no particular project.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU  General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  General Public License for more details.
*
* You should have received a copy of the GNU  General Public
* License along with this program; if not, write to the Free
* Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
*****/
/*****
* $Source$
*****/
/*****
* ChangeLog 
* $Log$
* Revision 1.1.7.1  1999/02/04 02:16:47  koen
* XmHTML-1.1.7 Beta, first release to GNOME CVS
*
*
*****/ 

#ifndef _tka_h_
#define _tka_h_

/*****
* Toolkit independent rendering functions. This enables us to use the
* same engine for rendering to a display, text or postscript.
* 
* This abstraction makes it a *lot* easier when porting XmHTML to other
* toolkits, provided the display functions know how to deal/convert the
* X-specific types.
*****/
#define GC_FILL_SOLID				0
#define GC_FILL_TILED				1
#define GC_FILL_STIPPLED			2
#define GC_FILL_OPAQUE_STIPPLED		3

#define GC_CAP_NOT_LAST				0
#define GC_CAP_BUTT					1
#define GC_CAP_ROUND				2
#define GC_CAP_PROJECTING			3

#define GC_LINE_SOLID				0
#define GC_LINE_ON_OFF_DASH			1
#define GC_LINE_DOUBLE_DASH			2

#define GC_JOIN_MITER				0
#define GC_JOIN_ROUND				1
#define GC_JOIN_BEVEL				2

#define GC_GXcopy					0

#define GC_COORDMODE_ORIGIN			0
#define GC_COORDMODE_PREVIOUS		1

typedef struct _ToolkitAbstraction{
	void *dpy;
	GdkWindow *win;
	Window DefaultRoot;

	/*****
	* Screen definitions
	*****/
	gint width;			/* width in pixels			*/
	gint height;		/* height in pixels			*/
	gint widthMM;		/* width in millimeters		*/
	gint heightMM;		/* height in millimeters	*/

	/**********
	* Xlib function wrappers
	**********/

	/*****
	* GC properties
	*****/

	gint fill_style[4];
	gint cap_style[4];
	gint line_style[3];
	gint join_style[3];
	gint gc_func[2];
	gint coord_mode[2];

	/*****
	* GC functions
	*****/
	GdkGC *(*CreateGC)();
	gint (*FreeGC)();
	gint (*CopyGC)();
	gint (*SetFunction)();
	gint (*SetClipMask)();
	gint (*SetClipOrigin)();
	gint (*SetTile)();
	gint (*SetTSOrigin)();
	gint (*SetFillStyle)();
	gint (*SetFont)();
	gint (*SetForeground)();
	gint (*SetBackground)();
	gint (*SetLineAttributes)();

	/*****
	* Font functions
	*****/
	GdkFont* (*LoadQueryFont)();
	gint (*FreeFont)();
	gint (*GetFontProperty)();

	/*****
	* Cursor & pointer functions
	*****/
	gint (*UngrabPointer)();
	gint (*DefineCursor)();
	gint (*UndefineCursor)();
	gint (*FreeCursor)();

	/*****
	* Color functions
	*****/

	gint (*ParseColor)();
	gint (*AllocColor)();
	gint (*QueryColor)();
	gint (*QueryColors)();
	gint (*FreeColors)();

	/*****
	* Pixmap functions
	*****/

	GdkPixmap *(*CreatePixmap)();
	GdkPixmap *(*CreatePixmapFromBitmapData)();
	gint    (*FreePixmap)();

	/*****
	* XImage functions
	*****/

	GdkImage *(*CreateImage)();
	void (*DestroyImage)();
	gint  (*PutImage)();
	GdkImage *(*GetImage)();

	gulong (*GetPixel)();
		
	/*****
	* string/text functions
	*****/
	gint (*TextWidth)();
	gint (*TextExtents)();
	gint (*DrawString)();

	/*****
	* Render functions
	*****/

	gint (*DrawLine)();
	gint (*DrawLines)();
	gint (*DrawRectangle)();
	gint (*FillRectangle)();
	gint (*DrawArc)();
	gint (*FillArc)();

	/*****
	* misc. functions
	*****/

	gint (*CopyArea)();
	gint (*ClearArea)();
	gint (*Sync)();

	/**********
	* X Toolkit Intrinsics wrappers
	**********/

	Boolean	(*IsRealized)();
	Boolean (*IsManaged)();
	void	(*ManageChild)();
	void	(*UnmanageChild)();
	void	(*MoveWidget)();
	void	(*ResizeWidget)();
	void	(*ConfigureWidget)();
	void    (*DestroyWidget)();
	void    (*SetMappedWhenManaged)();
	void	(*RemoveTimeOut)();
	glong	(*AddTimeOut)();

	/**********
	* Motif Wrappers
	**********/
	void	(*DrawShadows)();

	/**********
	* Implementation Specific data
	**********/
	void *data;
	void (*FreeData)();

}ToolkitAbstraction;

/* Create a new toolkit abstraction */
extern ToolkitAbstraction *XmHTMLToolkitAbstractionCreate(void);

/* destroy a toolkit abstraction */
extern void XmHTMLToolkitAbstractionDestroy(ToolkitAbstraction *tka);

/* Supply a new toolkit abstraction to a XmHTML Widget */
extern Boolean XmHTMLToolkitAbstractionSet(Widget w, ToolkitAbstraction *tka);

/* Don't add anything after this endif! */
#endif /* _tka_h_ */

