#ifndef lint
static char rcsId[]="$Header$";
#endif
/*****
* example_1.c : simple demonstration on how to use XmHTML
*
* This file Version	$Revision$
*
* Creation date:		Mon Jan 27 02:06:08 GMT+0100 1997
* Last modification: 	$Date$
* By:					$Author$
* Current State:		$State$
*
* Author:				newt
*
* Copyright (C) 1994-1997 by Ripley Software Development
* All Rights Reserved
*
* This file is part of the XmHTML Widget Library.
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU [Library] General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU [Library] General Public
* License along with this library; if not, write to the Free
* Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
*****/
/*****
* ChangeLog
* $Log$
* Revision 1.1.7.1  1999/02/04 02:14:51  koen
* XmHTML-1.1.7 Beta, first release to GNOME CVS
*
* Revision 1.6  1997/10/23 00:28:37  newt
* XmHTML Beta 1.1.0 release
*
* Revision 1.5  1997/05/28 02:02:49  newt
* Changes to reflect updated convenience function protos.
*
* Revision 1.4  1997/03/20 08:20:37  newt
* enlarged default with and height
*
* Revision 1.3  1997/03/11 20:08:55  newt
* Changed XmHTMLSetText to XmHTMLTextSet
*
* Revision 1.2  1997/03/04 01:02:45  newt
* Added printing of XmHTML version strings
*
* Revision 1.1  1997/02/11 01:58:25  newt
* Initial Revision
*
*****/

#include <stdio.h>
#include <stdlib.h>
#include <Xm/Xm.h>
#include <Xm/PushB.h>
#include <Xm/Frame.h>
#include <Xm/Form.h>

#include <XmHTML/XmHTML.h>
#include "debug.h"

/*** Private Variable Declarations ***/
static Widget html;

/*****
* Change this to change the application class of the examples
*****/
#define APP_CLASS		"HTMLDemos"

/*****
* Name: 		exitCB
* Return Type: 	void
* Description: 	callback for the exit button
* In: 
*	widget:		button widget id
*	client_data:unused
*	call_data:	unused
* Returns:
*
*****/
static void
exitCB(Widget widget, XtPointer client_data, XtPointer call_data)
{
	printf("Bye!\n");
	exit(EXIT_SUCCESS);
}

/*****
* Name: 		anchorCB
* Return Type: 	void
* Description: 	XmNactivateCallback for the XmHTML widget
* In: 
*	widget:		widget id, in this case that of the HTML widget 
*	client_data:data registered with callback, unused
*	cbs:		XmHTML callback structure.
* Returns:
*	nothing.
* Note:
*	We don't care what sort of url has been selected. 
*	Setting the doit field to True instructs XmHTML to do it's own scrolling. 
*	XmHTML is smart enought to only scroll to a location in this document if
*	it really exists.
*	Setting the visited field also to True will cause XmHTML to render the 
*	selected anchor as being visited.
*****/
static void
anchorCB(Widget widget, XtPointer client_data, 
	XmHTMLAnchorCallbackStruct *cbs)
{
	cbs->doit = True;
	cbs->visited = True;
}

/*****
* Name: 		loadFile
* Return Type: 	String
* Description: 	loads the contents of the given file.
* In: 
*	filename:	name of the file to load
* Returns:
*	contents of the loaded file.
*****/
static String
loadFile(String filename)
{
	FILE *file;
	int size;
	static String content;

	/* open the given file */
	if((file = fopen(filename, "r")) == NULL)
	{
		perror(filename);
		return(NULL);
	}

	/* see how large this file is */
	fseek(file, 0, SEEK_END);
	size = ftell(file);
	rewind(file);

	/* allocate a buffer large enough to contain the entire file */
	if((content = malloc(size+1)) == NULL)
	{
		fprintf(stderr, "malloc failed for %i bytes\n", size);
		exit(EXIT_FAILURE);
	}

	/* now read the contents of this file */
	if((fread(content, 1, size, file)) != size)
		printf("Warning: did not read entire file!\n");

	fclose(file);

	/* sanity */
	content[size] = '\0';

	return(content);
}

/*****
* Name:			main
* Return Type:	int
* Description:	main for example 1
* In:
*	argc:		no of arguments on command line
*	argv:		array of command line arguments
* Returns:
*	EXIT_FAILURE when an error occurs, EXIT_SUCCESS otherwise
* Note:
*	this example should be started with the name of a HTML file to display.
*****/
int
main(int argc, char **argv)
{
	XtAppContext context;
	Widget toplevel, form, frame, button;
	String content;

	fprintf(stderr, "%s, %i\n", XmHTMLVERSION_STRING, XmHTMLGetVersion());

	/* set the debugging levels */
	_XmHTMLSetDebugLevels(&argc, argv);

	/* check command line arguments, but allow for X11 options */
	if(argc < 2)
	{
		printf("%s: simple XmHTML example\n", argv[0]);
		printf("\tUsage: %s <filename>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	content = loadFile(argv[1]);

	/* create toplevel widget */
	toplevel = XtVaAppInitialize(&context, APP_CLASS, NULL, 0,
		&argc, argv, NULL, NULL, NULL);

	/* create a form as the main container */
	form = XtVaCreateWidget("form",
		xmFormWidgetClass, toplevel,
		NULL);

	/* create the exit button */
	button = XtVaCreateManagedWidget("Exit",
		xmPushButtonWidgetClass, form,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 10,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 10,
		NULL);

	/* add the exit callback */
	XtAddCallback(button, XmNactivateCallback, (XtCallbackProc)exitCB, 
		NULL);

	/* create a frame as the html container */
	frame = XtVaCreateManagedWidget("frame",
		xmFrameWidgetClass, form,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, button,
		XmNtopOffset, 10,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 10,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 10,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 10,
		XmNshadowType, XmSHADOW_IN,
		NULL);

	/* create the HTML widget using the default resources */
	html = XtVaCreateManagedWidget("html",
		xmHTMLWidgetClass, frame,
		XmNmarginWidth, 20,
		XmNmarginHeight, 20,
		XmNwidth, 600,
		XmNheight, 500,
		NULL);

	if(content == NULL)
		XmHTMLTextSetString(html, "<html><body>Could not read given "
			"file</body></html>");
	else
	{
		XmHTMLTextSetString(html, content);
		free(content);
	}

	/* add a simple anchor callback so XmHTML can jump to local anchors */
	XtAddCallback(html, XmNactivateCallback, 
		(XtCallbackProc)anchorCB, NULL);

	/* manage the form */
	XtManageChild(form);

	/* realize the main application */
	XtRealizeWidget(toplevel);

	/* The HTML widget has the focus */
	XmProcessTraversal(html, XmTRAVERSE_CURRENT);

	/* enter the event loop */
	XtAppMainLoop(context);

	/* never reached, but keeps compiler happy */
	exit(EXIT_SUCCESS);
}
