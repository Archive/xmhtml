#! /usr/bin/perl

# Test any form with this CGI script.
# formtest.cgi by David Efflandt (efflandt@xnet.com)

# For UNIX systems:
# Point the first line of script to your perl.
# Upload from DOS|Win as ASCII (not binary or zmodem).
# 'chmod 755 filename' from shell (see 'man chmod').

# HTML header
print "Content-type: text/html\n\n";
print "<html><head><title>Form Test</title></head>\n<body>\n";
print "<h1>Form Test Results</h1>\n";

# Print POST data if any
if ($ENV{'REQUEST_METHOD'} eq "POST") {
  read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
  print "<p><hr>\n<h2>POST Data</h2>\n";
  print "<h3>Raw STDIN:</h3>\n";
  print "<pre>\n$buffer\n</pre>\n";
  &listdata;
} 

# Print GET data (QUERY_STRING) if any
if ($ENV{'QUERY_STRING'}) {
  $buffer = $ENV{'QUERY_STRING'};
  print "<p><hr>\n<h2>GET data</h2>\n";
  print "<h3>Raw QUERY_STRING:</h3>\n";
  print "<pre>\n$buffer\n</pre>\n";
  &listdata;
}

# No Data response
unless ($buffer) {
  print "<p><hr>\n<h2>No Form Data Submitted</h2>\n";
  print "This script will display any form data submitted ";
  print "to it using the GET or POST method.\n";
  exit;
}

print "<p></body></html>\n";

# List the variables
sub listdata {
  print "<h3>Variables:</h3>\n";
  # Split the name-value pairs
  @pairs = split(/&/, $buffer);

  foreach $pair (@pairs) {
    ($name, $value) = split(/=/, $pair);

    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
    $name =~ tr/+/ /;
    $name =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;

  # Plain text values
  #  print "<p><b>$name:</b> <pre>$value</pre>\n";

  # HTML values
    print "<p>$name = $value\n";
  }
}
