#
# lint configuration. I use lclint.
#

LIBRARY=libhttp.a

# List of source, object and header files 
SRCS=HTTP.c cookie.c

OBJS=HTTP.o cookie.o

HEADERS=HTTP.h HTTPP.h
 
# Target to make
TARGET=$(LIBRARY)

# Subdirectories to visit
SUBDIRS=

# rule to create .o files from .c files 
.c.o:
	$(RM) $@
	$(CC) $(CFLAGS) $(CPPFLAGS) $(INCLUDES) -c $<

all: $(TARGET)

# targets to build

$(TARGET):: $(OBJS)  
	$(RM) $@ \ 
	$(AR) $@ $(OBJS)  
	$(RANLIB) $@

stamp-includes:
	@if [ -d ../include ]; then set +x; \
	else (set -x; mkdir ../include); fi
	@if [ -d ../include/XmHTML ]; then set +x; \
	else (set -x; mkdir ../include/XmHTML); fi
	@(set -x; cd ../include/XmHTML; for i in $(HEADERS); do \
		$(RM) $$i; \
		$(LN) ../../http/$$i .; \
	done)
	touch $@

includes:: stamp-includes

depend:: $(SRCS) 
	$(MAKEDEPEND) $(INCLUDES) $(CPPFLAGS) $(SRCS)

clean::
	$(RM) $(OBJS)
	$(RM) $(TARGET)

distclean:: clean 
	$(RM) core *.out *.log make.world *.bak *.last *.auto *.rej *.orig
	$(RM) *.lh *.lcs *.lint stamp-includes
	$(CP) Makefile.org Makefile

realclean:: distclean

#--------------------------------------------------------------------------
# don't delete anything below this line, makedepend depends on it
#--------------------------------------------------------------------------
