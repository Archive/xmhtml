/* simple_html.c -- Create a minimally configured XmHTML widget */
#include <XmHTML/XmHTML.h>

int
main(int argc, char **argv)
{
	Widget toplevel, html;
	XtAppContext app;

	toplevel = XtVaAppInitialize(&app, "Demos", NULL, 0,
		&argc, argv, NULL, NULL);

	html = XmCreateHTML(toplevel, "html", NULL, 0);

	/* All widgets returned by any XmCreateXXX function is unmanaged */
	XtManageChild(html);

	XmHTMLTextSetString(html, "<html><body>A minimally configured XmHTML "
		"widget.</body></html>");

	XtRealizeWidget(toplevel);
	XtAppMainLoop(app);
	return(0);
}
