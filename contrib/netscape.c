#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>

#define NS_VERSION      "_MOZILLA_VERSION"
#define NS_COMMAND      "_MOZILLA_COMMAND"
#define NETSCAPE        "Netscape"

extern Window WsNetscapeWindow(Display *display, Window window, char *name);
extern Window WsFindNetscapeWindow(Display *display, Window starting_window, char *name, Window (*compare_func)());
extern void WsSendNavigatorCommand(Display *display, Window window, char *command);


Window WsNetscapeWindow(Display *display, Window window, char *name)
{
   Atom type;
   int format;
   int status;
   unsigned long nitems, bytesafter;
   unsigned char **version=NULL;
   Window found=(Window)None;
   XClassHint *classhint;

   if (!name)
   {
      if ((XGetWindowProperty(display,
                              window,
                              XInternAtom (display, NS_VERSION, False),
                              0L,
                              (long)BUFSIZ,
                              False,
                              XA_STRING,
                              &type,
                              &format,
                              &nitems,
                              &bytesafter,
                              (unsigned char **)&version)) == Success &&
         *version && type != None)
      {
         found = window;
         XFree((char *)version);
      }
   }
   else
   {
      classhint = XAllocClassHint();
      if ((XGetClassHint(display, window, classhint)))
      {
         if (!strcmp(classhint->res_class, NETSCAPE) &&
             !strcmp(classhint->res_name, name)) found = window;
      }
      XFree((char *)classhint);
   }
   return (found);

}  /* WsNetscapeWindow */


Window WsFindNetscapeWindow(Display *display, Window starting_window,
                            char *name, Window (*compare_func)())
{

   Window rootwindow, window_parent;
   int i;
   unsigned int num_children=0;
   Window *children=NULL;
   Window window = (compare_func) (display, starting_window, name);

   if (window != (Window)None) return (window);

   if ((XQueryTree(display,
                   starting_window,
                   &rootwindow,
                   &window_parent,
                   &children,
                   &num_children)) == 0) return ((Window)None);

   i = 0;
   while (( i < num_children ) && ( window == (Window)None ))
      window = WsFindNetscapeWindow(display, children[i++], name, compare_func);
   if (children) XFree((char *)children);

   return(window);

}  /* WsFindNetscapeWindow */


void WsSendNavigatorCommand(Display *display, Window window, char *command)
{

   XChangeProperty(display,
                   window,
                   XInternAtom(display, NS_COMMAND, False),
                   XA_STRING,
                   8,
                   PropModeReplace,
                   (unsigned char *)command,
                   strlen(command)+1);
   XFlush(display);

}  /* WsSendNavigatorCommand */



/* Example stuff

     if ((window = WsFindNetscapeWindow(display,
                                        XDefaultRootWindow(display),
                                        name,
                                        WsNetscapeWindow)))
     {

        WsSendNavigatorCommand(display, window, command);
     }
     else
     {
        if (!(fork()))
        {
           if (execl("/bin/sh", "/bin/sh", "-c", cmd, NULL) == -1)
           {
              fprintf(stderr, "execl failed (%s)", strerror(errno));
              exit(100);
           }
        }
     }

*/

